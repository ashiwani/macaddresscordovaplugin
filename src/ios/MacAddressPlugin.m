

#import "MacAddressPlugin.h"
#import <SystemConfiguration/CaptiveNetwork.h>

@implementation MacAddressPlugin

- (void)getMacAddress:(CDVInvokedUrlCommand*)command {
    
    CDVPluginResult* pluginResult = nil;

    CFArrayRef myArray = CNCopySupportedInterfaces();
    CFDictionaryRef captiveNetWork = CNCopyCurrentNetworkInfo(CFArrayGetValueAtIndex(myArray, 0));
    NSDictionary *myDictionnary = (__bridge NSDictionary *)captiveNetWork;
    NSString *bssid = [myDictionnary objectForKey:@"BSSID"];
    
    if (bssid.length == 0) {
        
    pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:@""];

    }else{
       pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:bssid];
    }
    
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    
}

@end