
#import <Cordova/CDV.h>

@interface MacAddressPlugin : CDVPlugin

- (void)getMacAddress:(CDVInvokedUrlCommand*)command;

@end