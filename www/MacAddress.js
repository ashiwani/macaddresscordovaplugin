/*
 * MacAddress
 * Implements the javascript access to the cordova plugin for retrieving the router mac address. Returns @"" if router mac address not found.
 */

/*
 @return the mac address class instance
 */
 
 var MacAddress = {

 	getMacAddress: function(successCallback, failureCallback){
 		cordova.exec(successCallback, failureCallback, 'MacAddressPlugin',
 			'getMacAddress', []);
 	}
 };

 module.exports = MacAddress;